package com.tom_novak.android.nfiles.domain;

import java.util.ArrayList;
import java.util.List;

public class DrawerGroup implements Comparable<DrawerGroup> {

    private final DrawerGroupType type;
    private final List<DrawerItem> items = new ArrayList<>();

    public DrawerGroup(DrawerGroupType type) {
        this.type = type;
    }

    public DrawerGroupType getType() {
        return type;
    }

    public void addItem(DrawerItem item) {
        items.add(item);
    }

    public DrawerItem getItem(int position) {
        return items.get(position);
    }

    public boolean removeItem(DrawerItem item) {
        return items.remove(item);
    }

    public int getItemCount() {
        return items.size();
    }

    @Override
    public int compareTo(DrawerGroup another) {
        int thisOrder = this.getType().getOrder();
        int secondOrder = another.getType().getOrder();

        return thisOrder - secondOrder;
    }
}
