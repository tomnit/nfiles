package com.tom_novak.android.nfiles.thumbnail;

import android.graphics.Bitmap;

public interface IBitmapCache {

    public void addBitmap(String key, Bitmap bitmap);

    public Bitmap getBitmap(String key);
}
