package com.tom_novak.android.nfiles.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.io.File;

public class FileInfoDialogFragment extends DialogFragment {

    public static FileInfoDialogFragment newInstance(File file) {
        FileInfoDialogFragment fragment = new FileInfoDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable("File", file);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File file = (File)getArguments().getSerializable("File");

        if(file == null) {
            return null;
        }

        return new AlertDialog.Builder(getActivity())
                .setTitle("Ahoj")
                .create();
    }
}
