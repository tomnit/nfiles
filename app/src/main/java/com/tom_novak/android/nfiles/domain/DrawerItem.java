package com.tom_novak.android.nfiles.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class DrawerItem implements Parcelable {

    public static final Parcelable.Creator<DrawerItem> CREATOR
            = new Parcelable.Creator<DrawerItem>() {
        public DrawerItem createFromParcel(Parcel in) {
            return new DrawerItem(in);
        }

        public DrawerItem[] newArray(int size) {
            return new DrawerItem[size];
        }
    };
    
    private String title;
    private final DrawerGroupType type;
    private String path;

    public DrawerItem(DrawerGroupType type) {
        this.type = type;
    }

    public DrawerItem(DrawerGroupType type, String title, String path) {
        this(type);
        this.title = title;
        this.path = path;
    }

    private DrawerItem(Parcel in) {
        this.title = in.readString();
        this.type = (DrawerGroupType)in.readSerializable();
        this.path = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public DrawerGroupType getType() {
        return type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return title;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(title);
        out.writeSerializable(type);
        out.writeString(path);
    }
}
