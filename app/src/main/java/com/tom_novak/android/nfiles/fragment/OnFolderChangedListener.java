package com.tom_novak.android.nfiles.fragment;

public interface OnFolderChangedListener {
    boolean onFolderChanged(String changedFolderPath, boolean goingForward);
}
