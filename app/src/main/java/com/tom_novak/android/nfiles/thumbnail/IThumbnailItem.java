package com.tom_novak.android.nfiles.thumbnail;

import android.widget.ImageView;

import java.io.File;

public interface IThumbnailItem {

    void setThumbnail(File file, ImageView imageView);
}
