package com.tom_novak.android.nfiles.async;

import android.content.Context;
import android.os.AsyncTask;

import com.tom_novak.android.nfiles.R;
import com.tom_novak.android.nfiles.domain.DrawerGroupType;
import com.tom_novak.android.nfiles.domain.DrawerItem;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageItemsAsyncTask extends AsyncTask<Void, Void, List<DrawerItem>> {

    public interface IStorageItemsCallback {
        void onItemsListed(List<DrawerItem> items);
    }
    public static final String STORAGE_PATH = "/storage";
    public static final Map<String, Integer> deviceNameMap = new HashMap<>();

    static {
        deviceNameMap.put("usb", R.string.usb);
        deviceNameMap.put("sdcard", R.string.sdcard);
        deviceNameMap.put("emmc", R.string.emmc);
    }

    private final WeakReference<IStorageItemsCallback> callbackReference;
    private final Context context;

    public StorageItemsAsyncTask(Context context, IStorageItemsCallback callback) {
        this.context = context;
        this.callbackReference = new WeakReference<IStorageItemsCallback>(callback);
    }

    @Override
    protected List<DrawerItem> doInBackground(Void... params) {
        List<DrawerItem> navItems = null;
        File storageFolder = new File(STORAGE_PATH);
        if(storageFolder.exists()) {
            String[] itemPaths = storageFolder.list();
            if(itemPaths.length > 0) {
                navItems = new ArrayList<>();
            }
            File itemFile = null;
            for(String itemPath : itemPaths) {
                itemFile = new File(storageFolder.getAbsolutePath() + "/" + itemPath);
                if(itemFile.canRead() && itemFile.canExecute()) {
                    String deviceName = itemFile.getName();
                    for(String key : deviceNameMap.keySet()) {
                        if(deviceName.contains(key)) {
                            deviceName = context.getResources().getString(deviceNameMap.get(key));
                        }
                    }
                    navItems.add(new DrawerItem(
                            DrawerGroupType.DEVICE,
                            deviceName,
                            itemFile.getAbsolutePath()));
                }
            }
        }
        return navItems;
    }

    @Override
    protected void onPostExecute(List<DrawerItem> drawerItems) {
        if(callbackReference != null && drawerItems != null) {
            IStorageItemsCallback callback = callbackReference.get();
            if(callback != null) {
                callback.onItemsListed(drawerItems);
            }
        }
    }
}
