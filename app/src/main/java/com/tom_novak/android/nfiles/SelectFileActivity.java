package com.tom_novak.android.nfiles;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.tom_novak.android.nfiles.fragment.FolderFragment;
import com.tom_novak.android.nfiles.fragment.IFolderFragment;
import com.tom_novak.android.nfiles.fragment.OnFolderChangedListener;


public class SelectFileActivity extends ActionBarActivity implements OnFolderChangedListener {

    public static final String DEFAULT_FOLDER_KEY = "defaultFolder";
    public static final String FACTORY_DEFAULT_FOLDER_PATH = "/";

    private IFolderFragment folderFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_file);

        folderFragment = (IFolderFragment)getSupportFragmentManager()
                .findFragmentById(R.id.select_file_folder_fragment);
        folderFragment.addOnFolderChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        folderFragment.setFolder(FACTORY_DEFAULT_FOLDER_PATH);
    }

    @Override
    public boolean onFolderChanged(String changedFolderPath, boolean goingForward) {
        return false;
    }
}
