package com.tom_novak.android.nfiles.thumbnail;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class ImageThumbnail implements IThumbnailItem {

    public static final int THUMBNAIL_SIZE = 64;

    private Map<ImageView, AsyncTask> thumbnailTaskMap = new HashMap<>();
    private IBitmapCache bitmapCache;

    public ImageThumbnail(IBitmapCache bitmapCache) {
        this.bitmapCache = bitmapCache;
    }

    @Override
    public void setThumbnail(File file, ImageView imageView) {
        cancelLastAsyncTask(imageView);
        final Bitmap thumbnail = bitmapCache.getBitmap(file.getAbsolutePath());
        if(thumbnail == null) {
            thumbnailTaskMap.put(imageView, new ThumbnailAsyncTask(imageView).execute(file));
        } else {
            imageView.setImageBitmap(thumbnail);
        }
    }

    private void cancelLastAsyncTask(ImageView imageView) {
        AsyncTask task = thumbnailTaskMap.get(imageView);
        if(task != null) {
            AsyncTask.Status status = task.getStatus();
            if (status == AsyncTask.Status.PENDING || status == AsyncTask.Status.RUNNING) {
                task.cancel(true);
            }
        }
    }

    private class ThumbnailAsyncTask extends AsyncTask<File, Void, Bitmap> {

        private final WeakReference<ImageView> imageViewReference;

        public ThumbnailAsyncTask(ImageView imageView) {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(File... params) {
            if(params.length > 0) {
                final String absolutePath = params[0].getAbsolutePath();
                // create thumbnail from bitmap;
                ThumbnailUtils utils = new ThumbnailUtils();
                final Bitmap thumbnail = utils.extractThumbnail(
                        BitmapFactory.decodeFile(absolutePath), THUMBNAIL_SIZE, THUMBNAIL_SIZE);
                bitmapCache.addBitmap(absolutePath, thumbnail);

                return thumbnail;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(imageViewReference != null && bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                if(imageView != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }
}
