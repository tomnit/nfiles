package com.tom_novak.android.nfiles.thumbnail;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.util.LruCache;

public class BitmapCache extends LruCache<String, Bitmap> implements IBitmapCache {

    /**
     * @param maxSize for caches that do not override {@link #sizeOf}, this is
     *                the maximum number of entries in the cache. For all other caches,
     *                this is the maximum sum of the sizes of the entries in this cache.
     */
    public BitmapCache(int maxSize) {
        super(maxSize);
    }

    @Override
    public void addBitmap(String key, Bitmap bitmap) {
        if (key != null && bitmap != null && getBitmap(key) == null) {
            put(key, bitmap);
        }
    }

    @Override
    public Bitmap getBitmap(String key) {
        return get(key);
    }

    @Override
    protected int sizeOf(String key, Bitmap value) {
        if(Build.VERSION.SDK_INT >= 12) {
            return value.getByteCount() / 1024;
        } else {
            return value.getHeight() * value.getRowBytes() / 1024;
        }
    }
}
