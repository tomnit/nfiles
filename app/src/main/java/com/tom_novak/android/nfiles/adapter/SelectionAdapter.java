package com.tom_novak.android.nfiles.adapter;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.widget.ArrayAdapter;

public class SelectionAdapter<T> extends ArrayAdapter<T> implements ISelectionAdapter {
	
	private Map<Integer, Boolean> selections = new HashMap<Integer, Boolean>();
	
	public SelectionAdapter(Context context, int resId) {
		super(context, resId);
	}
	

	@Override
	public void setSelected(int position, boolean selected) {
		selections.put(position, selected == true ? true : null);	
	}
	

	@Override
	public boolean isSelected(int position) {
		Boolean result = selections.get(position);
		return result == null ? false : result;
	}

}
