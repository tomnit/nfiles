package com.tom_novak.android.nfiles;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tom_novak.android.nfiles.fragment.FileInfoDialogFragment;

import java.io.File;

public class CABManager {

    private AbsListView.MultiChoiceModeListener listener;
    private ListView list;

    public CABManager(AbsListView.MultiChoiceModeListener listener) {
        this.listener = listener;

    }

    public void initCAB(ListView list) {
        list.setOnItemLongClickListener(new NFolderListItemLongClickListener());
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        list.setMultiChoiceModeListener(listener);
    }

    private class NFolderListItemLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view,
                                       int position, long id) {
            ((AbsListView)parent).setItemChecked(position, true);
            return false;
        }
    }

}
