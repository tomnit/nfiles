package com.tom_novak.android.nfiles;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.tom_novak.android.nfiles.domain.DrawerItem;
import com.tom_novak.android.nfiles.fragment.IFolderFragment;
import com.tom_novak.android.nfiles.fragment.NavigationDrawerFragment;
import com.tom_novak.android.nfiles.fragment.OnFolderChangedListener;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallback, OnFolderChangedListener {

    public static final String DEFAULT_FOLDER_KEY = "defaultFolder";
    public static final String LAST_FOLDER_KEY = "lastFolder";
    public static final String FACTORY_DEFAULT_FOLDER_PATH = "/";

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;

//    protected String lastFolderPath = FACTORY_DEFAULT_FOLDER_PATH;
    protected boolean isGoingForward = false;
    private String lastFolderPath;
    protected IFolderFragment activeFolderFragment;
    private boolean onBackExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activeFolderFragment = (IFolderFragment)getSupportFragmentManager()
                .findFragmentById(R.id.main_folder_column);
        activeFolderFragment.addOnFolderChangedListener(this);

        initDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initFolderFragments();
    }

    private void initFolderFragments() {
        showFolder(lastFolderPath);
    }

    private void initDrawer() {
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

    }

    @Override
    public void onNavigationDrawerItemSelected(DrawerItem navItem) {
        if(navItem != null) {
            if (activeFolderFragment != null) {
                activeFolderFragment.setFolder(navItem.getPath());
            }
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.activity_main_actions, menu);
            restoreActionBar();
            return true;
        }
        // init menu items
        MenuItem hiddenFilesMenuItme = menu.findItem(R.id.main_action_hidden_files);
        if(hiddenFilesMenuItme != null) {
            hiddenFilesMenuItme.setChecked(activeFolderFragment.isVisibleHiddenFiles());
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_action_hidden_files:
                item.setChecked(!item.isChecked());
                activeFolderFragment.setVisibleHiddenFiles(item.isChecked());
                activeFolderFragment.refresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LAST_FOLDER_KEY, activeFolderFragment.getFolderPath());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lastFolderPath = savedInstanceState.getString(LAST_FOLDER_KEY);
    }

    public String getFolderPath(String folderPathKey) {
        return getPreferences(MODE_PRIVATE).getString(folderPathKey, FACTORY_DEFAULT_FOLDER_PATH);
    }


    public void showFolder(String folderPath) {
        // show default folder if path is null
        if(folderPath == null) {
            activeFolderFragment.setFolder(getFolderPath(DEFAULT_FOLDER_KEY));
            return;
        }

        // dont change fragment if path is same as last used
        if(activeFolderFragment.getFolderPath().equals(folderPath)) {
            activeFolderFragment.refresh();
            return;
        }

        activeFolderFragment.setFolder(folderPath);
    }


    @Override
    public void onBackPressed() {
        if(onBackExit) {
            onBackExit = false;
            super.onBackPressed();
        } else {
            if(!activeFolderFragment.goBack()) {
                onBackExit = true;
                Toast.makeText(this, R.string.exit_on_back_pressed, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onFolderChanged(String changedFolderPath, boolean goingForward) {
        if(goingForward) {
            onBackExit = false;
        }
        lastFolderPath = changedFolderPath;
        return true;
    }
}
