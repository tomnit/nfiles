package com.tom_novak.android.nfiles.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.tom_novak.android.nfiles.R;
import com.tom_novak.android.nfiles.domain.DrawerGroup;
import com.tom_novak.android.nfiles.domain.DrawerItem;

import java.util.List;

public class NavigationDrawerAdapter extends BaseExpandableListAdapter {

    private final LayoutInflater inflater;
    private final Context context;
    private final List<DrawerGroup> groups;

    public NavigationDrawerAdapter(Context context, List<DrawerGroup> groups) {
        this.context = context;
        this.groups = groups;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getItemCount();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getItem(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.view_expandable_group_item, null);
            holder.title = ((TextView)convertView.findViewById(android.R.id.text1));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        DrawerGroup group = groups.get(groupPosition);
        holder.title.setText(group.getType().getTitleResource());

        ExpandableListView expandableListView = (ExpandableListView)parent;
        expandableListView.expandGroup(groupPosition);
        expandableListView.setGroupIndicator(null);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.view_expandable_child_item, null);
            holder.title = ((TextView)convertView.findViewById(android.R.id.text1));
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        DrawerItem item = groups.get(groupPosition).getItem(childPosition);
        holder.title.setText(item.getTitle());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder {
        public TextView title;
    }
}
