package com.tom_novak.android.nfiles.thumbnail;

import android.os.AsyncTask;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import com.tom_novak.android.nfiles.R;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FileIconManager {

    public static final int DEFAULT_FILE_ICON_RES = R.drawable.ic_file;

    private final Map<String, IThumbnailItem> thumbnailMap = new HashMap<>();
    private final Map<String, Integer> placeholderMap = new HashMap<>();

    private MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

    public FileIconManager(IBitmapCache bitmapCache) {
        IThumbnailItem imageThumbnailItem = new ImageThumbnail(bitmapCache);
        thumbnailMap.put("image/jpg", imageThumbnailItem);
        thumbnailMap.put("image/png", imageThumbnailItem);
        thumbnailMap.put("image/jpeg", imageThumbnailItem);
    }

    public void addThumbnailItem(String mimeType, IThumbnailItem thumbnailItem) {
        thumbnailMap.put(mimeType, thumbnailItem);
    }

    public IThumbnailItem removeThumbnailItem(String mimeType) {
        return thumbnailMap.remove(mimeType);
    }

    public boolean hasThumbnailItem(String mimeType) {
        return thumbnailMap.containsKey(mimeType);
    }

    public boolean hasThumbnailItem(File file) {
        return hasThumbnailItem(getMimeType(file));
    }

    public String getMimeType(File file) {
        String[] splittedPath = file.getAbsolutePath().split("\\.");
        String mimeType = null;
        if(splittedPath.length > 0) {
            String extension = splittedPath[splittedPath.length - 1];
            if(extension != null && !extension.equals("")) {
                mimeType = mimeTypeMap.getMimeTypeFromExtension(extension);
            }
        }
        return mimeType;
    }

    public void setIcon(File file, ImageView imageView) {
        imageView.setImageResource(DEFAULT_FILE_ICON_RES);
        // set thumbnail
        IThumbnailItem thumbnailItem = thumbnailMap.get(getMimeType(file));
        if(thumbnailItem != null) {
            thumbnailItem.setThumbnail(file, imageView);
        }
    }
}
