package com.tom_novak.android.nfiles.adapter;

import java.io.File;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.tom_novak.android.nfiles.R;
import com.tom_novak.android.nfiles.fragment.FolderFragment;
import com.tom_novak.android.nfiles.thumbnail.FileIconManager;
import com.tom_novak.android.nfiles.thumbnail.IBitmapCache;


public class FolderItemArrayAdapter extends SelectionAdapter<File> {

	public static final int FOLDER_IMAGE_RESOURCE = R.drawable.ic_folder;

	private final FolderFragment fragment;
	private final FileIconManager iconManager;

	public FolderItemArrayAdapter(FolderFragment fragment, IBitmapCache bitmapCache) {
		super(fragment.getActivity(), R.layout.view_list_item);
		this.fragment = fragment;
		iconManager = new FileIconManager(bitmapCache);
	}
	
	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.view_list_item, null);

			holder.image = ((ImageView) convertView.findViewById(R.id.list_item_image));
			holder.title = ((TextView) convertView.findViewById(R.id.list_item_text));
			holder.check = ((CheckBox) convertView.findViewById(R.id.list_item_check));

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set icon for file/folder
		File item = getItem(position);
		if (item.isFile()) {
			iconManager.setIcon(item, holder.image);
		} else {
			holder.image.setImageResource(FOLDER_IMAGE_RESOURCE);
		}
		holder.title.setText(item.getName());

		// show grayscale icon for non readable items
		if (!item.canRead()) {
			ColorMatrix colorMatrix = new ColorMatrix();
			colorMatrix.setSaturation(0);

			ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
			holder.image.setColorFilter(colorFilter);
		} else {
			holder.image.setColorFilter(null);
		}

		// enable/disable listview item
		convertView.setClickable(!item.canRead());

		// set darker gray background for non readable files/folders
		Resources res = fragment.getActivity().getResources();
		convertView.setBackgroundColor(!item.canRead() ? res.getColor(R.color.light_gray) : 0);

		// show checked items
		boolean isChecked = ((AbsListView) parent).isItemChecked(position);
		holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				((AbsListView) parent).setItemChecked(position, isChecked);
			}
		});
		holder.check.setChecked(isChecked);

		return convertView;
	}

	static class ViewHolder {
		public ImageView image;
		public TextView title;
		public CheckBox check;
	}

}
