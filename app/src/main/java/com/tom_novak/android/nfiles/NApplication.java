package com.tom_novak.android.nfiles;

import android.app.Application;

import com.tom_novak.android.nfiles.thumbnail.BitmapCache;

public class NApplication extends Application {

    private BitmapCache thumbnailCache;

    @Override
    public void onCreate() {
        initCache();
    }

    public BitmapCache getBitmapCache() {
        return thumbnailCache;
    }

    private void initCache() {
        final int maxMemory = (int) Runtime.getRuntime().maxMemory() / 1024;
        final int cacheSize = maxMemory / 8;
        this.thumbnailCache = new BitmapCache(cacheSize);
    }
}
