package com.tom_novak.android.nfiles.adapter;

public interface ISelectionAdapter {
	
	void setSelected(int position, boolean selected);
	
	boolean isSelected(int position);

}
