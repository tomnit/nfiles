package com.tom_novak.android.nfiles.fragment;

public interface IFolderFragment {

	void setFolder(String folderPath);

	String getFolderPath();
	
	void refresh();

	boolean goBack();

	void addOnFolderChangedListener(OnFolderChangedListener listener);

	void removeOnFolderChangedListener(OnFolderChangedListener listener);

	boolean isVisibleHiddenFiles();

	void setVisibleHiddenFiles(boolean visibleHiddenFiles);
}
