package com.tom_novak.android.nfiles.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tom_novak.android.nfiles.CABManager;
import com.tom_novak.android.nfiles.NApplication;
import com.tom_novak.android.nfiles.R;
import com.tom_novak.android.nfiles.adapter.FolderItemArrayAdapter;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class FolderFragment extends Fragment
		implements IFolderFragment, AdapterView.OnItemClickListener, OnFolderChangedListener {

	public static final String PATH_KEY = "folderPath";

	public static Fragment newInstance(String folderPath) {
		Fragment fragment = new FolderFragment();

		// add folder path argument to fragment
		Bundle extra = new Bundle();
		extra.putString(FolderFragment.PATH_KEY, folderPath);
		fragment.setArguments(extra);

		return fragment;
	}

	private CABManager cabManager = new CABManager(new NMultiModeChoiceModeLlistener());

	private TextView pathView;
	private View headerView;
	private ListView list;
	private ArrayAdapter<File> adapter;
	
	private MimeTypeMap mimeMap = MimeTypeMap.getSingleton();
	private Activity activity;

	private FolderItemsAsyncTask lastTask;
	private Stack<String> backStack = new Stack<>();
	private List<OnFolderChangedListener> onFolderChangedListeners = new ArrayList<>();

	private boolean visibleHiddenFiles = false;
	private boolean visibleFolderUpNavigation = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_folder_list, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		this.list = (ListView)view.findViewById(android.R.id.list);
		this.pathView = (TextView)view.findViewById(R.id.folder_path);
		list.setOnItemClickListener(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		activity = getActivity();
		NApplication application = (NApplication)activity.getApplication();
		
		setRetainInstance(true);

		headerView = getActivity().getLayoutInflater().inflate(R.layout.view_simple_icon_list_item, null);
		adapter = new FolderItemArrayAdapter(this, application.getBitmapCache());

		if(visibleFolderUpNavigation) {
			list.addHeaderView(headerView);
		}
		list.setAdapter(adapter);

		Bundle arguments = getArguments();
		if(arguments != null) {
			setFolder(arguments.getString(PATH_KEY));
		}
		cabManager.initCAB(list);
	}
	
	@Override
	public void setFolder(String folderPath) {
		String lastPath = null;
		if(!backStack.empty()) {
			lastPath = backStack.peek();
		}
		if(!folderPath.equals(lastPath)) {
			backStack.push(folderPath);
			callOnFolderChanged(backStack.peek(), true);
			refresh();
		}
	}

	@Override
	public String getFolderPath() {
		return backStack.empty() ? null : backStack.peek();
	}

	@Override
	public void refresh() {
		cancelLastTask();
		
//		@SuppressWarnings("unchecked")
//		ArrayAdapter<File> adapter = (ArrayAdapter<File>)list.();

		// set file filter
		FileFilter filter = null;
		if(!isVisibleHiddenFiles()) {
			filter = new HiddenFileFilter();
		}
		
		if(adapter != null) {
			lastTask = new FolderItemsAsyncTask(adapter, filter);
			lastTask.execute(backStack.peek());
		}

		pathView.setText(backStack.peek());
	}

	@Override
	public boolean goBack() {
		if(backStack.size() <= 1) {
			return false;
		}

		backStack.pop();
		callOnFolderChanged(backStack.peek(), false);
		refresh();
		return true;
	}

	@Override
	public void addOnFolderChangedListener(OnFolderChangedListener listener) {
		this.onFolderChangedListeners.add(listener);
	}

	@Override
	public void removeOnFolderChangedListener(OnFolderChangedListener listener) {
		this.onFolderChangedListeners.remove(listener);
	}

	public boolean isVisibleHiddenFiles() {
		return visibleHiddenFiles;
	}

	public void setVisibleHiddenFiles(boolean visibleHiddenFiles) {
		this.visibleHiddenFiles = visibleHiddenFiles;
	}

	private void callOnFolderChanged(String changedFolderPath, boolean goingForward) {
		boolean changeWorked = false;
		for(OnFolderChangedListener onFolderChangedListener : onFolderChangedListeners) {
			changeWorked  = changeWorked || onFolderChangedListener.onFolderChanged(changedFolderPath, goingForward);
		}
		if(!changeWorked) {
			onFolderChanged(changedFolderPath, goingForward);
		}
	}

	@Override
	public boolean onFolderChanged(String changedFolderPath, boolean goingForward) {
		// TODO
		return false;
	}

	private void cancelLastTask() {
		if(lastTask != null) {
			lastTask.cancel(true);
		}	
	}

	public void onHeaderViewClick() {}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(position < list.getHeaderViewsCount()) {
			onHeaderViewClick();
			return;
		}
		position = position + list.getHeaderViewsCount();
		File file = (File)list.getItemAtPosition(position);
		if(file.canRead()) {
			if(file.isDirectory()) {
				setFolder(file.getAbsolutePath());
			} else {
				Uri fileUri = Uri.fromFile(file);

				Intent intent = new Intent(Intent.ACTION_VIEW);

				String mimeType = mimeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(fileUri.toString()));
				if(mimeType == null) {
					Toast.makeText(getActivity(), getActivity().getString(R.string.cannot_find_application_to_open), Toast.LENGTH_SHORT).show();
				} else {
					intent.setDataAndType(fileUri, mimeType);

					PackageManager packageManager = getActivity().getPackageManager();
					List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);

					Intent chooser = Intent.createChooser(intent, "Choose");

					if(intent.resolveActivity(getActivity().getPackageManager()) != null) {
						startActivity(chooser);
					}
				}
			}
		} else {
			Toast.makeText(getActivity(), getActivity().getString(R.string.operation_not_allowed), Toast.LENGTH_SHORT).show();
		}
	}

	public static class HiddenFileFilter implements FileFilter {
		@Override
		public boolean accept(File pathname) {
			return !pathname.isHidden();
		}
	}

	public static class FolderItemsAsyncTask extends AsyncTask<String, Void, List<File>> {

		public static final String TAG = "com.tom_novak.android.nfilemanager.async.FolderItemsAsyncTask";

		private ArrayAdapter<File> adapter;
		private FileFilter fileFilter;

		public FolderItemsAsyncTask(ArrayAdapter<File> adapter, FileFilter fileFilter) {
			this.adapter = adapter;
			this.fileFilter = fileFilter;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			adapter.clear();

		}

		@Override
		protected List<File> doInBackground(String... params) {
			if(params.length > 0) {
				File folder = new File(params[0]);
				if(folder.exists() && folder.isDirectory()) {
					return new ArrayList<File>(Arrays.asList(folder.listFiles(fileFilter)));
				}
			}
			return new ArrayList<File>();
		}

		@Override
		protected void onPostExecute(List<File> result) {
			super.onPostExecute(result);

			for(File item : result) {
				adapter.add(item);
			}
		}
	}

	private class NMultiModeChoiceModeLlistener implements AbsListView.MultiChoiceModeListener {

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.cab_actions, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
				case R.id.cab_action_info:
	                showFileInfo(new File("/"));
					mode.finish(); // Action picked, so close the CAB
					return true;
				case R.id.cab_action_delete:
					deleteCheckedItems();
					Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
					// TODO
				default:
					return false;
			}
		}

		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position,
											  long id, boolean checked) {
			// TODO exist better solution?
			list.invalidateViews();

			mode.setTitle(list.getCheckedItemCount() + " items selected");
		}

		private void showFileInfo(File file) {
			FileInfoDialogFragment dialogFragment = FileInfoDialogFragment.newInstance(file);
			dialogFragment.show(getActivity().getSupportFragmentManager(), "dialog");
		}

		private void deleteCheckedItems() {
			SparseBooleanArray checkedItemPositions = list.getCheckedItemPositions();
			for(int i = 0, j = checkedItemPositions.size(); i < j; i++) {
				int position = checkedItemPositions.keyAt(i);
				if(checkedItemPositions.get(position)) {
					File file = adapter.getItem(position);
					file.delete();
				}
			}
			list.invalidateViews();
		}
	}

}
