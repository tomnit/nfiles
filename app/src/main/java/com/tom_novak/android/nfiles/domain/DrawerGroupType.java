package com.tom_novak.android.nfiles.domain;

import com.tom_novak.android.nfiles.R;

public enum DrawerGroupType {

    PLACE(R.string.places, 0),
    DEVICE(R.string.devices, 1),
    BOOKMARK(R.string.bookmarks, 2),
    NETWORK(R.string.network, 3),
    CLOUD(R.string.cloud, 4);

    private int title;
    private int order;

    DrawerGroupType(int title, int order) {
        this.title = title;
        this.order = order;
    }

    public int getTitleResource() {
        return title;
    }

    public int getOrder() {
        return this.order;
    }
}
